﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterGenerator : MonoBehaviour
{
	public int grid_size;
	public float plane_size;
	public GameObject water;
	public GameObject player;
	private WaterPlaneGenerator water_generator;
	private WaterController water_controller;
	private Vector3 last_generation_position;
	private List<GameObject> waters = new List<GameObject>();
	private Vector3 waters_position;

	private void Awake()
	{
		water_generator = water.GetComponent<WaterPlaneGenerator>();
		water_controller = water.GetComponent<WaterController>();
		water_generator.size = plane_size;
		water_generator.grid_size = grid_size;
		last_generation_position = player.transform.position;
		last_generation_position.y = 0;
		
		CreateWaters();
	}

	private void CreateWaters()
	{
		waters_position = player.transform.position - (new Vector3(plane_size, 0, plane_size)*1f);
		waters_position.y = 0;
		for(int y=0; y < 3; y++)
		{
			for(int x=0; x < 3; x++)
			{
				waters.Add(Instantiate(water, waters_position + (new Vector3(plane_size*x-(4*x), 0, plane_size*y-(4*y))), Quaternion.identity, this.transform));
			}
		}
	}


	private void Update()
	{
        Vector3 down = transform.TransformDirection(Vector3.down);
		RaycastHit hit;

		if(Physics.Raycast(player.transform.position, down, out hit))
		{
			// If it's water
			if(hit.transform.IsChildOf(this.transform))
			{
				// get index in array
				int water_index = waters.FindIndex(w => w==hit.transform.gameObject);
				Debug.Log(water_index);
			}
		}
		 
	}
}
