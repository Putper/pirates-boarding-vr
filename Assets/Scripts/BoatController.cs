﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatController : MonoBehaviour
{
	public GameObject player;
	// public GameObject controller;
	public float speed = 1.5f;
	// private SteamVR_TrackedObject tracked_object;
	new private Rigidbody rigidbody;
	private Vector3 velocity;


	void Start()
	{
		//  tracked_object = controller.GetComponent<SteamVR_TrackedObject>();
		 rigidbody = this.GetComponent<Rigidbody>();
	}


	// private SteamVR_Controller.Device device
	// {
	// 	get { return SteamVR_Controller.Input((int)tracked_object.index); }
	// }

	void Update()
	{
		player.transform.position = this.transform.position;
		rigidbody.velocity = velocity;
	}
	

	void FixedUpdate()
	{
		velocity = rigidbody.velocity;
        // if( device.GetHairTrigger() )
        // {
		// 	Vector3 aim_pos = controller.transform.forward * speed;
		// 	velocity += new Vector3(aim_pos.x, rigidbody.velocity.y, aim_pos.z);
		// }
	}


	private void OnTriggerStay(Collider collider)
	{
		if(collider.transform.parent.name == "Water")
		{
			Debug.Log("you in water man");
			velocity.y += 1;
		}
	}
}
